Emphasis, aka italics, with *asterisks* or _underscores_.
Strong emphasis, aka bold, with **asterisks** or __underscores__.
Strikethrough uses two tildes. ~~Scratch this.~~
Add image link  and you can update image use [Image uploader service](doc/1git/imageuploadservice.md)

![](https://gitlab.com/picbed/bed/uploads/75985eac80cb11269120d0283ce6a8a5/logo.png)
